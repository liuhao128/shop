package com.qf.myshop.shop_entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("product")
public class Product implements java.io.Serializable{
    @TableId(value = "p_id", type = IdType.AUTO)
    private Integer id;
    @TableField("t_id")
    private Integer tid;
    @TableField("p_name")
    private String name;
    @TableField("p_image")
    private String image;
    @TableField("p_time")
    private Date time;
    @TableField("p_price")
    private Double price;
    @TableField("p_state")
    private Integer state;
    @TableField("p_info")
    private String info;
}