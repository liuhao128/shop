package com.qf.myshop.shop_entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user")
public class User implements java.io.Serializable{
    @TableId(value = "u_id", type = IdType.AUTO)
    private Integer id;
    @TableField("u_name")
    private String name;
    @TableField("u_password")
    private String password;
    @TableField("u_email")
    private String email;
    @TableField("u_sex")
    private String sex;
    @TableField("u_status")
    private Integer status;
    @TableField("u_code")
    private String code;
    @TableField("u_role")
    private Integer role;
    @TableField("u_salt")
    private String salt;
}