package com.qf.myshop.shop_product.service.impl;

import com.qf.myshop.shop_entity.Product;
import com.qf.myshop.shop_entity.ResultData;
import com.qf.myshop.shop_product.dao.ProductDAO;
import com.qf.myshop.shop_product.service.ProductService;
import com.qf.myshop.shop_utils.MessageCodeConstants;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Resource
    private ProductDAO productDAO;

    @Override
    public ResultData findAll() {
        final List<Product> list = productDAO.selectList(null);
        if (list == null || list.size() == 0){
            return ResultData.createFailJsonResult(MessageCodeConstants.PRODUCT_LIST_NO_DATA, "没有查询到商品列表数据");
        }
        return ResultData.createSuccessJsonResult(list);
    }
}
