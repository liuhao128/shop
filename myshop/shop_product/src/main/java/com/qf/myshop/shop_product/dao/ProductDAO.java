package com.qf.myshop.shop_product.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.myshop.shop_entity.Product;

public interface ProductDAO extends BaseMapper<Product> {
}
