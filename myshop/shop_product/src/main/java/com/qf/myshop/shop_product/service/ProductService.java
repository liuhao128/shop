package com.qf.myshop.shop_product.service;

import com.qf.myshop.shop_entity.ResultData;

public interface ProductService {
    ResultData findAll();
}
