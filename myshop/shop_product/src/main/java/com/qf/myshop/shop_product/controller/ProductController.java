package com.qf.myshop.shop_product.controller;

import com.qf.myshop.shop_entity.ResultData;
import com.qf.myshop.shop_product.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ProductController {
    @Resource
    private ProductService productService;

    @GetMapping("/list")
    public ResultData list(){
        return productService.findAll();
    }
}
