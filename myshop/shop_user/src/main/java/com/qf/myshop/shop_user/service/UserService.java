package com.qf.myshop.shop_user.service;

import com.qf.myshop.shop_entity.ResultData;
import com.qf.myshop.shop_entity.User;

public interface UserService {
    ResultData save(User user);

    ResultData login(User user);

    ResultData check(String token);
}
