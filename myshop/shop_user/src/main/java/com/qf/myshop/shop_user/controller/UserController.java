package com.qf.myshop.shop_user.controller;

import com.qf.myshop.shop_entity.ResultData;
import com.qf.myshop.shop_entity.User;
import com.qf.myshop.shop_user.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {
    @Resource
    private UserService userService;

    @PostMapping("/add")
    public ResultData register(User user){
        return userService.save(user);
    }

    @PostMapping("/login")
    public ResultData login(User user){
        return userService.login(user);
    }

    @GetMapping("/check")
    public ResultData check(String token){
        return userService.check(token);
    }
}
