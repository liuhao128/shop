package com.qf.myshop.shop_user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.myshop.shop_entity.User;

public interface UserDAO extends BaseMapper<User> {
}
