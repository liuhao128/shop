// 全局配置默认路由
// axios.defaults.baseURL = 'http://192.168.0.157:8989/xxxx/xxx/';

// axios请求拦截器
axios.interceptors.request.use(function (config) {
    // 这里的config包含每次请求的内容
    let token = window.sessionStorage.getItem('token')
    if (token) {
        // 添加headers
        config.headers.token = `${token}`;
        // config.headers['content-type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
    } else {}
    return config;
}, function (err) {
    return Promise.reject(err);
})


// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if(response.data.code=="10000"){
        return response;
    }else{
        if(response.data.code=="11010701"){
            alert(response.data.desc);
            location.href = "/login.html";
        }else{
            alert(response.data.desc);
        }
    }

    return response;
}, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
});