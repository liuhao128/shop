package com.qf.myshop.shop_zuul.service;

import com.qf.myshop.shop_entity.ResultData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("shop-user")
public interface UserService {
    @GetMapping("/check")
    ResultData check(@RequestParam("token") String token);
}
