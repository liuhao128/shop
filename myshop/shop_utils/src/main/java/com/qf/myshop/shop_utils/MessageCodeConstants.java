package com.qf.myshop.shop_utils;

public interface MessageCodeConstants {
    // 规则：服务编号（11）+模块编号（01）+方法编号（01）+异常编号（01）
    String REGISTER_FAIL = "11010101";
    String LOGIN_FAIL = "11010601";
    String LOGIN_TIME_OUT = "11010701";
    String PRODUCT_LIST_NO_DATA = "12010401";
}
